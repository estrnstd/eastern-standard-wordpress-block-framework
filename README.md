# Eastern Standard WordPress Block Framework
A WordPress plugin to standardize working with blocks.

## Requirements

This plugin requires these plugins to be installed and activated:

* <a href="https://www.advancedcustomfields.com/pro" target="_blank">Advanced Custom Fields Pro (>=6.0.0))</a>
* <a href="https://bitbucket.org/estrnstd/eastern-standard-wordpress-utilities/src/v1" target="_blank">Eastern Standard WordPress Utilities (^1.0.7)</a>
* <a href="https://bitbucket.org/estrnstd/eastern-standard-wordpress-field-group-framework/src/v2" target="_blank">Eastern Standard WordPress Field Group Framework (^2.1.0)</a>

## Installation
Copy the contents of the `eswp-block-framework` directory from this repository into the plugins directory. The path should look like this: `/wp-content/plugins/eswp-block-framework/eswp-block-framework.php`.

Activate the plugin via the WordPress dashboard 'Plugins' page (`/wp-admin/plugins.php`).

## Usage

Using the `init` hook, call the `ESWP_Block_Framework::initialize_blocks` method passing in a directory or array of directories containing a block `config.php` file. The `initialize_blocks` method accepts `*` wild cards within the argument strings.

```php
add_action('init', function () {
	if (class_exists('ESWP_Block_Framework')) {
		$block_data = ESWP_Block_Framework::initialize_blocks(get_template_directory() . '/custom-blocks/*');
	}
	else {
		trigger_error(__("Class 'ESWP_Block_Framework' does not exist. Make sure the 'Eastern Standard WordPress Block Framework' plugin is installed and activated."), E_USER_WARNING);
	}
});
```

### Creating Blocks

Included in this repository is an example block. You can find it located at the `'example-block'` directory. It's a good reference for a very basic block, but keep in mind: for existing projects, it's **strongly recommended** that you follow any conventions present in the project you're working in. Many projects will have a slightly different look to how their blocks are organized, so it might be easier to copy an existing block rather than start from this example block. For example, it's common for projects to have a compile process for CSS and JS files which would lend itself to a different directory structure than the one shown in the example block.

## Updates
This plugin can be updated from the WordPress dashboard, read about it in the "Updating Plugins" section of the updater plugin readme: <a href="https://bitbucket.org/estrnstd/eastern-standard-wordpress-updater/src/v1" target="_blank">Eastern Standard WordPress Updater</a> (you will need access to view this repository).

## Disabling Updates

This plugin can have it's dashboard updates disabled, read about it in the "Disabling Plugin Updates" section of the updater plugin readme: <a href="https://bitbucket.org/estrnstd/eastern-standard-wordpress-updater/src/v1" target="_blank">Eastern Standard WordPress Updater</a> (you will need access to view this repository).

## Development

The versioning for this plugin follows the <a href="https://semver.org" target="_blank">semver</a> guidelines.

When committing updates, remember to update the version in `eswp-block-framework.php` and `info.json`, then compress the `eswp-block-framework` directory into a new `eswp-block-framework.zip` file. These steps ensure the WordPress plugin dashboard update process will work as expected.
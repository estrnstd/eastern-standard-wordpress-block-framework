<?php

$name = 'acf/example-block';

return [
	// Required. Define the path to the block.json file.
	'block_json_path' => __DIR__ . '/block.json',
	// Optional. Set custom metadata. These values will override values from the block.json file during block registration.
	// https://developer.wordpress.org/block-editor/reference-guides/block-api/block-metadata
	'metadata' => [
		'acf' => [
			'blockVersion' => 2,
			'mode' => 'auto',
			'renderTemplate' => __DIR__ . '/template.php',
		],
	],
	// Optional. Register styles and add the handles to the metadata.
	// https://developer.wordpress.org/reference/functions/wp_register_style
	'metadata_styles' => [
		$name . '-style' => [ // Required. wp_register_style $handle argument.
			'metadata_key' => 'style', // Required. One of the block metadata style keys: 'style' or 'editorStyle'.
			'src' => __DIR__ . '/styles/style.css', // Required. wp_register_style $src argument.
			'deps' => [], // Optional. wp_register_style $deps argument.
			'ver' => ['modified-time'], // Optional. wp_register_style $ver argument or array with eswp custom value.
			'media' => 'all', // Optional. wp_register_style $media argument.
		],
		$name . '-editor-style' => [ // Required. wp_register_style $handle argument.
			'metadata_key' => 'editorStyle', // Required. One of the block metadata style keys: 'style' or 'editorStyle'.
			'src' => __DIR__ . '/styles/editor-style.css', // Required. wp_register_style $src argument.
			'deps' => [], // Optional. wp_register_style $deps argument.
			'ver' => ['modified-time'], // Optional. wp_register_style $ver argument or array with eswp custom value.
			'media' => 'all', // Optional. wp_register_style $media argument.
		],
	],
	// Optional. Register scripts and add them to the metadata.
	// https://developer.wordpress.org/reference/functions/wp_register_script
	'metadata_scripts' => [
		$name . '-script' => [ // Required. wp_register_script $handle argument.
			'metadata_key' => 'script', // Required. One of the block metadata script keys: 'script', 'viewScript', or 'editorScript'.
			'src' => __DIR__ . '/scripts/script.js', // Required. wp_register_script $src argument.
			'deps' => [], // Optional. wp_register_script $deps argument.
			'ver' => ['modified-time'], // Optional. wp_register_script $ver argument or array with eswp custom value.
			'in_footer' => false, // Optional. wp_register_script $in_footer argument.
			'localize' => [ // Optional. https://developer.wordpress.org/reference/functions/wp_localize_script
				'object_name' => null, // Required. wp_localize_script $object_name argument.
				'l10n' => null, // Required. wp_localize_script $l10n argument.
			],
		],
		$name . '-editor-script' => [ // Required. wp_register_script $handle argument.
			'metadata_key' => 'editorScript', // Required. One of the block metadata script keys: 'script', 'viewScript', or 'editorScript'.
			'src' => __DIR__ . '/scripts/editor-script.js', // Required. wp_register_script $src argument.
			'deps' => [], // Optional. wp_register_script $deps argument.
			'ver' => ['modified-time'], // Optional. wp_register_script $ver argument or array with eswp custom value.
			'in_footer' => false, // Optional. wp_register_script $in_footer argument.
			'localize' => [ // Optional. https://developer.wordpress.org/reference/functions/wp_localize_script
				'object_name' => null, // Required. wp_localize_script $object_name argument.
				'l10n' => null, // Required. wp_localize_script $l10n argument.
			],
		],
		$name . '-view-script' => [ // Required. wp_register_script $handle argument.
			'metadata_key' => 'viewScript', // Required. One of the block metadata script keys: 'script', 'viewScript', or 'editorScript'.
			'src' => __DIR__ . '/scripts/view-script.js', // Required. wp_register_script $src argument.
			'deps' => [], // Optional. wp_register_script $deps argument.
			'ver' => ['modified-time'], // Optional. wp_register_script $ver argument or array with eswp custom value.
			'in_footer' => false, // Optional. wp_register_script $in_footer argument.
			'localize' => [ // Optional. https://developer.wordpress.org/reference/functions/wp_localize_script
				'object_name' => null, // Required. wp_localize_script $object_name argument.
				'l10n' => null, // Required. wp_localize_script $l10n argument.
			],
		],
	],
	// Optional. Define the field(s) for this block.
	// https://www.advancedcustomfields.com/resources/register-fields-via-php
	'field_group' => [
		'fields' => [
			[
				'label' => 'Example Field',
				'name' => 'example_field',
				'type' => 'text',
				'instructions' => 'This is an example text field',
				'required' => 0,
				'default_value' => '',
				'placeholder' => '',
				'prepend' => '',
				'append' => '',
				'maxlength' => '',
			],
		],
	],
	// Optional. Set which field(s) should be included in the excerpt.
	'excerpt_fields' => [
		'example_field',
	],
];

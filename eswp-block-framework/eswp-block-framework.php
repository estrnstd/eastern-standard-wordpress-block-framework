<?php
/*
Plugin Name: Eastern Standard WordPress Block Framework
Plugin URI: https://bitbucket.org/estrnstd/eastern-standard-wordpress-block-framework
Description: A system for configuring custom blocks.
Version: 2.1.0
Author: Eastern Standard
Author URI: https://easternstandard.com
Update URI: eswp-block-framework
*/

require_once(ABSPATH . 'wp-admin/includes/plugin.php');

$eswp_dependency_manager_path = ABSPATH . 'wp-content/plugins/eswp-dependency-manager/eswp-dependency-manager.php';
if (file_exists($eswp_dependency_manager_path)) {
	include_once $eswp_dependency_manager_path;
}

if (!class_exists('ESWP_Block_Framework')) {
	#[AllowDynamicProperties]
	class ESWP_Block_Framework {

		public static $initialized = false;
		public static $block_configs = [];
		public static $currently_registering_block_config_key = null;
		public static $excerpt_fields = [];

		private static function initialize() {
			if (!self::$initialized) {
				add_filter('block_type_metadata', [__CLASS__, 'block_type_metadata_filter']);
				self::$initialized = true;
			}
		}

		public static function block_type_metadata_filter($metadata) {
			if (self::$currently_registering_block_config_key) {
				$metadata = array_merge($metadata, self::$block_configs[self::$currently_registering_block_config_key]['metadata']);
				self::$block_configs[self::$currently_registering_block_config_key]['merged_metadata'] = $metadata;
				self::$currently_registering_block_config_key = null;
			}
			return $metadata;
		}

		private static function normalize_block_directories($block_directories) {
			$input_valid = true;
			$normalized_block_directories = [];

			if (is_string($block_directories) || is_array($block_directories)) {
				if (is_string($block_directories)) {
					$block_directories = [$block_directories];
				}
				if (is_array($block_directories)) {
					foreach ($block_directories as $block_directory) {
						if (!is_string($block_directory)) {
							$input_valid = false;
							break;
						}
					}
				}
			}
			else {
				$input_valid = false;
			}

			if (!$input_valid) {
				trigger_error(__("Class 'ESWP_Block_Framework', method 'normalize_block_directories', parameter: '\$block_directories' must be a string or an array of strings."), E_USER_WARNING);
				return $normalized_block_directories;
			}

			// Handle globs
			foreach ($block_directories as $key => $block_directory) {
				if (str_contains($block_directory, '*')) {
					array_splice($block_directories, $key, 1, glob($block_directory));
				}
			}

			// Remove trailing slashes
			foreach ($block_directories as &$block_directory) {
				$block_directory = rtrim($block_directory, '/');
			}

			$normalized_block_directories = $block_directories;
			return $normalized_block_directories;
		}

		private static function initialize_metadata_styles(&$block_config) {
			if (array_key_exists('metadata_styles', $block_config)) {
				if (!is_array($block_config['metadata_styles'])) {
					trigger_error(__("Invalid block config in: '{$block_config['block_config_path']}', '\$config['metadata_styles'] must be an array."), E_USER_WARNING);
				}
				else {
					foreach ($block_config['metadata_styles'] as $handle => $metadata_style) {
						if (!is_array($metadata_style)) {
							trigger_error(__("Invalid block config in: '{$block_config['block_config_path']}', '\$config['metadata_styles'] items must all be arrays."), E_USER_WARNING);
						}
						else {
							if ($metadata_style['metadata_key'] !== 'style' && $metadata_style['metadata_key'] !== 'editorStyle') {
								trigger_error(__("Invalid block config in: '{$block_config['block_config_path']}', '\$config['metadata_styles']['{$handle}']['metadata_key'] must be 'style' or 'editorStyle'."), E_USER_WARNING);
							}
							else {
								// Assign variables for cleaner code.
								$metadata_key = $metadata_style['metadata_key'];
								$src = $metadata_style['src'];
								$deps = $metadata_style['deps'];
								$ver = $metadata_style['ver'];
								$media = $metadata_style['media'];
								// Preprocess $ver value.
								if (is_array($ver)) {
									if (count($ver) === 1 && $ver[0] === 'modified-time') {
										$ver = filemtime(eswp_get_internal_path($src));
									}
									else {
										trigger_error(__("Invalid block config in: '{$block_config['block_config_path']}', '\$config['metadata_styles']['{$handle}']['ver'] invalid eswp custom array value."), E_USER_WARNING);
										$ver = false;
									}
								}
								// Register style.
								wp_register_style($handle, $src, $deps, $ver, $media);
								// Add handle to metadata.
								if (!array_key_exists('metadata', $block_config)) {
									$block_config['metadata'] = [];
								}
								if (!array_key_exists($metadata_key, $block_config['metadata'])) {
									$block_config['metadata'][$metadata_key] = [];
								}
								if (is_string($block_config['metadata'][$metadata_key])) {
									$block_config['metadata'][$metadata_key] = [$block_config['metadata'][$metadata_key]];
								}
								if (is_array($block_config['metadata'][$metadata_key])) {
									array_push($block_config['metadata'][$metadata_key], $handle);
								}
							}
						}
					}
				}
			}
		}

		private static function initialize_metadata_scripts(&$block_config) {
			if (array_key_exists('metadata_scripts', $block_config)) {
				if (!is_array($block_config['metadata_scripts'])) {
					trigger_error(__("Invalid block config in: '{$block_config['block_config_path']}', '\$config['metadata_scripts'] must be an array."), E_USER_WARNING);
				}
				else {
					foreach ($block_config['metadata_scripts'] as $handle => $metadata_script) {
						if (!is_array($metadata_script)) {
							trigger_error(__("Invalid block config in: '{$block_config['block_config_path']}', '\$config['$metadata_script'] items must all be arrays."), E_USER_WARNING);
						}
						else {
							if ($metadata_script['metadata_key'] !== 'script' && $metadata_script['metadata_key'] !== 'editorScript' && $metadata_script['metadata_key'] !== 'viewScript') {
								trigger_error(__("Invalid block config in: '{$block_config['block_config_path']}', '\$config['metadata_scripts']['{$handle}']['metadata_key'] must be 'script', 'editorScript' or 'viewScript'."), E_USER_WARNING);
							}
							else {
								// Assign variables for cleaner code.
								$metadata_key = $metadata_script['metadata_key'];
								$src = $metadata_script['src'];
								$deps = [];
								if (isset($metadata_script['deps'])) {
									$deps = $metadata_script['deps'];
								}
								$ver = false;
								if (isset($metadata_script['ver'])) {
									$ver = $metadata_script['ver'];
								}
								// Preprocess $ver value.
								if (is_array($ver)) {
									if (count($ver) === 1 && $ver[0] === 'modified-time') {
										$ver = filemtime(eswp_get_internal_path($src));
									}
									else {
										trigger_error(__("Invalid block config in: '{$block_config['block_config_path']}', '\$config['metadata_scripts']['{$handle}']['ver'] invalid eswp custom array value."), E_USER_WARNING);
										$ver = false;
									}
								}
								$in_footer = false;
								if (isset($metadata_script['in_footer'])) {
									$in_footer = $metadata_script['in_footer'];
								}
								// Register script.
								wp_register_script($handle, $src, $deps, $ver, $in_footer);
								// Add handle to metadata.
								if (!array_key_exists('metadata', $block_config)) {
									$block_config['metadata'] = [];
								}
								if (!array_key_exists($metadata_key, $block_config['metadata'])) {
									$block_config['metadata'][$metadata_key] = [];
								}
								if (is_string($block_config['metadata'][$metadata_key])) {
									$block_config['metadata'][$metadata_key] = [$block_config['metadata'][$metadata_key]];
								}
								if (is_array($block_config['metadata'][$metadata_key])) {
									array_push($block_config['metadata'][$metadata_key], $handle);
								}
								// Localize data.
								if (isset($metadata_script['localize']) && is_array($metadata_script['localize'])
								&& isset($metadata_script['localize']['object_name']) && is_string($metadata_script['localize']['object_name'])
								&& isset($metadata_script['localize']['l10n'])) {
									$object_name = $metadata_script['localize']['object_name'];
									$l10n = $metadata_script['localize']['l10n'];
									wp_localize_script($handle, $object_name, $l10n);
								}
							}
						}
					}
				}
			}
		}

		private static function initialize_block_fields(&$block_config) {
			if (class_exists('ESWP_Field_Group_Framework') && array_key_exists('field_group', $block_config)) {
				$default = [];
				$merged_metadata_name = $block_config['merged_metadata']['name'];
				if (is_string($merged_metadata_name)) {
					$default['key'] = $merged_metadata_name;
					$default['location'] = [
						[
							[
								'param' => 'block',
								'operator' => '==',
								'value' => $merged_metadata_name,
							],
						],
					];
				}
				$merged_metadata_title = $block_config['merged_metadata']['title'];
				if (is_string($merged_metadata_title)) {
					$default['title'] = $merged_metadata_title;
				}
				$block_config['field_group'] = ESWP_Field_Group_Framework::normalize_field_group($block_config['field_group'], $block_config['block_config_path'], $default);
				acf_add_local_field_group($block_config['field_group']);
			}
		}

		/**
		 * initialize_blocks
		 * 
		 * Performs block data bootstrapping, then block registration.
		 * 
		 * @param   mixed  $block_directories  Required. A string or array of strings representing paths to block directories.
		 * @return  void
		 */

		public static function initialize_blocks($block_directories) {
			if (!self::$initialized) {
				trigger_error(__("Class 'ESWP_Block_Framework', method 'initialize_blocks' cannot be called before 'ESWP_Block_Framework' is initialized."), E_USER_WARNING);
				return null;
			}

			$block_directories = self::normalize_block_directories($block_directories);
			foreach ($block_directories as $block_directory) {

				$block_config_path = $block_directory . '/config.php';
				$block_config = include $block_config_path;

				if (is_array($block_config)) {
					$block_config['block_config_path'] = $block_config_path;
					$block_config['block_directory'] = $block_directory;

					self::initialize_metadata_styles($block_config);
					self::initialize_metadata_scripts($block_config);

					self::$block_configs[$block_directory] = $block_config;
				}

			}

			foreach (self::$block_configs as $key => &$block_config) {
				self::$currently_registering_block_config_key = $key;
				register_block_type($block_config['block_json_path']);
				self::initialize_block_fields($block_config);
				$block_name = $block_config['merged_metadata']['name'];
				if (array_key_exists('excerpt_fields', $block_config) && is_array($block_config['excerpt_fields'])) {
					self::$excerpt_fields[$block_name] = $block_config['excerpt_fields'];
				}
			}

			return [
				'block_configs' => self::$block_configs,
				'excerpt_fields' => self::$excerpt_fields,
			];
		}

		public function __construct() {

			$this->file = __FILE__;
			$this->dir = __DIR__;
			$this->plugin_data = get_plugin_data(__FILE__);
			$this->slug = plugin_basename(__DIR__);
			$this->basename_path = $this->slug . '/' . $this->slug . '.php';
			$this->dashboard_updating = true;
			$this->info_url = 'https://bitbucket.org/estrnstd/eastern-standard-wordpress-block-framework/raw/v2/info.json';
			$this->info_cache_allowed = true;
			$this->info_cache_key = $this->slug . '-info';
			$this->info_cache_length = DAY_IN_SECONDS;
			$this->plugin_dependencies = [
				[
					'name' => 'Advanced Custom Fields Pro', // Optional. Readable name of the plugin.
					'url' => 'https://www.advancedcustomfields.com/pro', // Optional. Link to the plugin.
					'path' => 'advanced-custom-fields-pro/acf.php', // Required. Path to the plugin relative to the plugins directory.
					'version' => '>=6.0.0', // Required. Semver constraints.
				],
				[
					'name' => 'Eastern Standard WordPress Utilities', // Optional. Readable name of the plugin.
					'url' => 'https://bitbucket.org/estrnstd/eastern-standard-wordpress-utilities/src/v1', // Optional. Link to the plugin.
					'path' => 'eswp-utilities/eswp-utilities.php', // Required. Path to the plugin relative to the plugins directory.
					'version' => '^1.2.6', // Required. Semver constraints.
				],
				[
					'name' => 'Eastern Standard WordPress Field Group Framework', // Optional. Readable name of the plugin.
					'url' => 'https://bitbucket.org/estrnstd/eastern-standard-wordpress-field-group-framework/src/v2', // Optional. Link to the plugin.
					'path' => 'eswp-field-group-framework/eswp-field-group-framework.php', // Required. Path to the plugin relative to the plugins directory.
					'version' => '^2.1.0', // Required. Semver constraints.
				],
			];

			if (class_exists('ESWP_Updater')) {
				ESWP_Updater::initialize_plugin_updates($this);
			}
			else {
				add_action('plugin-eswp-updater:initialized', function() {
					ESWP_Updater::initialize_plugin_updates($this);
				});
			}

			if (class_exists('ESWP_Dependency_Manager')) {
				$valid_dependencies = ESWP_Dependency_Manager::check_plugin_dependencies($this);
				if ($valid_dependencies) {
					self::initialize();
				}
			}
			else {
				self::initialize();
			}

		}

	}

	new ESWP_Block_Framework();

}
